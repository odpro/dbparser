﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dbParser
{
    public class Parser
    {
        const int VENUE_NAME_LENGTH = 6;
        const int MAX_VALUE_FOR_INT_ID = 100000; // !!!!!!!!!!!!!!!!!!!!!! start value ??????
        const string CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";


        public string generateDeclarationScriptPart(List<MyDictionary> list)
        {
            StringBuilder sb = new StringBuilder();
            for (var i = 0; i < list.Count; i++)
            {
                sb.AppendLine(String.Format("declare {0} {1} = {2} --{3}", list[i].name, list[i].variableType, list[i].newValue, list[i].oldValue));
            }
            sb.AppendLine("--                 ");
            return sb.ToString();

        }

        public string replaseText(string text, List<MyDictionary> list)
        {
            for (var i = 0; i < list.Count; i++)
            {
                text = text.Replace(list[i].oldValue, list[i].name);
            }
            return text;
        }

        public List<MyDictionary> getKeyValueVariableDictionary(string fieldName, ISet<string> valuesList)
        {
            List<MyDictionary> list = new List<MyDictionary>();
            switch (fieldName)
            {
                case "ItemId":
                    var newIntValues = getRandomIntArray(valuesList.Count);
                    for (var i = 0; i < newIntValues.Length; i++)
                    {
                        string key = "@" + fieldName + i;
                        string newValue = newIntValues[i].ToString();
                        list.Add(new MyDictionary("int", key, valuesList.ElementAt(i), newValue));
                    }
                    break;

                case "MMCode":
                    var newStringValues = getRandomIntArray(valuesList.Count);
                    for (var i = 0; i < newStringValues.Length; i++)
                    {
                        string key = "@" + fieldName + i;
                        string newValue = "'" + newStringValues[i].ToString() + "'";
                        list.Add(new MyDictionary("nvarchar", key, valuesList.ElementAt(i), newValue));
                    }
                    break;

                default:
                    var newGuidsValue = getRandomGuidsArray(valuesList.Count);
                    for (var i = 0; i < newGuidsValue.Length; i++)
                    {
                        string key = "@" + fieldName + i;
                        string newValue = "'" + newGuidsValue[i].ToString() + "'";
                        list.Add(new MyDictionary("uniqueidentifier", key, valuesList.ElementAt(i), newValue));
                    }
                    break;

            }


            return list;
        }


        private Guid[] getRandomGuidsArray(int count)
        {
            Guid[] guids = new Guid[count];
            for (var i = 0; i < count; i++)
            {
                guids[i] = Guid.NewGuid();
            }

            return guids;
        }


        private int[] getRandomIntArray(int count)
        {
            int[] arr = new int[count];
            Random r = new Random();
            for (var i = 0; i < count; i++)
            {
                arr[i] = r.Next(MAX_VALUE_FOR_INT_ID);
            }

            return arr;
        }

        private string[] getRandomStringArray(int count)
        {
            string[] arr = new string[count];
            for (var i = 0; i < count; i++)
            {
                arr[i] = generateRandomString(VENUE_NAME_LENGTH);
            }

            return arr;
        }

        private string generateRandomString(int length)
        {

            var random = new Random();
            return new string(Enumerable.Repeat(CHARS, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public ISet<string> getUniqueValuesList(string fieldName, string text)
        {

            HashSet<string> uniqueValuesList = new HashSet<string>();
            string[] lines = text.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            for (var i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains(fieldName))
                {
                    var value = getValueFromLine(fieldName, lines[i]);
                    uniqueValuesList.Add(value);
                }
            }

            return uniqueValuesList;
        }


        private string getValueFromLine(string fieldName, string line)
        {
            string valuesSelector = "VALUES (";
            int valuesSubstringStartIndex = line.IndexOf(valuesSelector) + valuesSelector.Length;

            string descriptionQueryPart = line.Substring(line.IndexOf('('), line.IndexOf(')') - line.IndexOf('('));
            string valuesQueryPart = line.Substring(valuesSubstringStartIndex, line.LastIndexOf(')') - valuesSubstringStartIndex);

            var descriptionArr = descriptionQueryPart.Split(',');
            var valuesArr = valuesQueryPart.Split(',');

            var totalCount = descriptionArr.Length;

            for (var i = 0; i < descriptionArr.Length; i++)
            {
                if (descriptionArr[i].Contains(fieldName))
                {
                    return valuesArr[i].Trim();
                }
            }

            return "";
        }


        /* private string getValueFromLine(string fieldName, string line)
         {
             var st = line.Split(',');
             var totalCount = st.Length;

             for (var i = 0; i < st.Length; i++)
             {
                 if (st[i].Contains(fieldName))
                 {
                     var startIndex = st[i].IndexOf("N'");
                     if (startIndex > -1)
                     {
                         st[totalCount / 2 + i].Substring(startIndex);
                     }
                     return st[totalCount / 2 + i];
                 }
             }

             return "";
         }*/


    }
}
