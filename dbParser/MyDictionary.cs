﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dbParser
{
    public class MyDictionary
    {
        public string name;
        public string oldValue;
        public string newValue;
        public string variableType;

        public MyDictionary()
        {

        }

        public MyDictionary(string variableType, string name, string oldValue, string newValue)
        {
            this.variableType = variableType;
            this.name = name;
            this.oldValue = oldValue;
            this.newValue = newValue;
        }
    }
}
