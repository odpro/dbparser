﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dbParser
{
    class Program
    {
        private static string[] fieldNames = { "VenueId", "MeetingSpaceId", "BuildingId", "FloorId", "MapId", "ItemId", "ResourceId", "ObjectId", "ModelId" };

        static void Main(string[] args)
        {
            Console.WriteLine("Enter sql file name");
            var fileName = Console.ReadLine();
            if (string.IsNullOrEmpty(fileName))
                return;

            if (!File.Exists(fileName))
                return;

            string fileContent = File.ReadAllText(fileName);
            string convertedScript = GenerateScript(fileContent);

            var directory = Path.GetDirectoryName(fileName);

            using (var fs = File.CreateText(Path.Combine(directory, string.Format("script-{0}.sql", DateTime.Now.ToString("hh-mm-ss-fff")))))
            {
                fs.Write(convertedScript);
            }

            Console.WriteLine("Successfuly converted. Press any key to exit");
            Console.ReadKey();

        }

        static string GenerateScript(string content)
        {

            Parser parser = new Parser();
            List<MyDictionary> allValuesList = new List<MyDictionary>();
            for (var i = 0; i < fieldNames.Length; i++)
            {
                var uniqueValuesList = parser.getUniqueValuesList(fieldNames[i], content);
                var valuesList = parser.getKeyValueVariableDictionary(fieldNames[i], uniqueValuesList);
                allValuesList.AddRange(valuesList);
            }

            var declarationText = parser.generateDeclarationScriptPart(allValuesList);
            var updatedText = parser.replaseText(content, allValuesList);

            var finalScript = declarationText + Environment.NewLine + updatedText;

            return finalScript;
        }
    }
}
